Canonicalizer
=============

Some tool to canonicalize string.

## Install

```
composer require pressop/canonicalizer
```

## License

This bundle is under the MIT license. See the complete license:

    LICENSE

<?php

/*
 * This file is part of the pressop/canonicalizer package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Canonicalizer;

/**
 * Class Canonicalizer
 *
 * @author Benjamin Georgeault
 */
class Canonicalizer implements CanonicalizerInterface
{
    /**
     * @inheritDoc
     */
    public function canonicalize(string $value = null): ?string
    {
        if (null === $value) {
            return null;
        }

        $encoding = mb_detect_encoding($value);

        return $encoding
            ? mb_convert_case($value, MB_CASE_LOWER, $encoding)
            : mb_convert_case($value, MB_CASE_LOWER)
        ;
    }
}

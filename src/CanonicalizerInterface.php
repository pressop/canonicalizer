<?php

/*
 * This file is part of the pressop/canonicalizer package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Canonicalizer;

/**
 * Interface CanonicalizerInterface
 *
 * @author Benjamin Georgeault
 */
interface CanonicalizerInterface
{
    /**
     * @param null|string $value
     * @return null|string
     */
    public function canonicalize(string $value = null): ?string;
}
